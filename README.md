#Global Game Jam 2020

https://kecece.herokuapp.com

- Create or join a room
- Start the game
- Draw
- Caption the drawing
- Repeat !
- Show the evolution !

Have fun :D

## Install
```
- npm install
- node app.js
- connect to [your_ip]:8002
```

**All players need to be on the same network !**

## Warning
- https://kecece.herokuapp.com/
- http://kecece.ml/

On a dedicated server, the network can have some issues ...

## TODO
- ~~Afficher le nombre de joueurs pret/joueurs total~~
- ~~Afficher le nombre de round/round total~~
- ~~Transition entre show finale => afficher le nom du joueur ayant recu le premier mot~~
- ~~Afficher qui a dessiné/decrit à la fin~~
- Ajouter des mots
- ~~Changer label pour plus simple~~
- Refactoring
- ~~Ajouter id player~~
- Ajouter un timer !
- Liste des salles rejoignables + total
