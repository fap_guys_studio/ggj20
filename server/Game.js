const util = require('util')
const EVENT_RESPONSE_CREATE_ROOM = 'rep_create_room'
const EVENT_START_DRAW = 'start_draw'
const EVENT_START_DESCRIPTION = 'start_description'
const EVENT_END_GAME = 'end_game'
const EVENT_FINISH_NEXT = 'finish_next'
const EVENT_PLAYER_VALIDATED = 'validated_player'
const EVENT_NEW_PLAYER = 'new_player'
const EVENT_DISCONNECTION = 'disconnect_player'

class Game {
  constructor(drawing, socket, room) {
    this._drawing = drawing
    this._idRoom = room

    this._players = []
    this._playerFinish = 0
    this._playerFinishShowed = -1
    this._round = 0
    this._ingame = false
    this.addPlayer(socket)
    this._repCreateRoom(socket)
    console.log(`create room ${this._idRoom}`)
  }

  addPlayer(socket) {
    socket.room = this._idRoom
    if (this._ingame) {
      console.log('Game already start !')
      return
    }
    socket.datas = []
    if (!this._players.includes(socket)) {
      this._players.push(socket)
      console.log(`add player ${socket.playerName} in room ${this._idRoom}`)

      this._sendAllPLayersList()
    }
  }

  removePlayer(socket) {
    console.log(`remove player ${socket.playerName} in room ${this._idRoom}`)
    socket.room = null
    if (this._players.includes(socket)) {
      this._players.splice(this._players.indexOf(socket), 1)
    }

    if (this._ingame) {
      console.warn('Error disconnect, end game')
      this._players.forEach(p => {
        p.room = null
        p.send(JSON.stringify({action: EVENT_DISCONNECTION, playerName: socket.playerName}))
      })
      this._players = []
    } else {
      this._sendAllPLayersList()
      return this._players.length
    }
  }

  _startDraw() {
    this._newRound()

    console.log('start draw')

    for (let num = 0; num < this._players.length; num++) {
      const pred = (num + this._players.length - 1) % this._players.length
      const predPlayer = this._players[pred]
      this._players[num].send(JSON.stringify({
        action: EVENT_START_DRAW,
        description: predPlayer.datas[this._round - 1],
        round: this._round
      }))
      console.log(`${this._players[num].playerName} draw ${predPlayer.datas[this._round - 1]} round ${this._round}`)
    }
    this._playerFinish = 0
  }

  _startDescription() {
    this._newRound()

    console.log('start description')

    for (let num = 0; num < this._players.length; num++) {
      const pred = (num + this._players.length - 1) % this._players.length
      const predPlayer = this._players[pred]

      this._players[num].send(JSON.stringify({
        action: EVENT_START_DESCRIPTION,
        drawing: predPlayer.datas[this._round - 1],
        round: this._round
      }))
      console.log(`${this._players[num].playerName} descr ${predPlayer.datas[this._round - 1].substring(0, 5)} round ${this._round}`)
    }
  }

  _newRound() {
    console.log('new round')
    this._round++
    this._playerFinish = 0
    this._players.forEach(p => p.hasValidated = false)
  }

  _startFinish() {
    console.log('game finish')

    this._ingame = false
    this._currentFinishPlayerIndex = -1
    this._startShowNextFinish()
  }

  _startShowNextFinish() {
    this._playerFinishShowed++
    this._currentFinishPlayerIndex++
    console.log('new show ' + this._currentFinishPlayerIndex)

    if (this._currentFinishPlayerIndex === this._players.length) {
      console.log('end game')

      this._players.forEach(p => {
        p.send(JSON.stringify({action: EVENT_END_GAME}))
      })
      this._players = []
      return
    }

    this._currentNextFinishData = -2
    this.nextShowFinish()
  }

  _sendAllPLayersList() {
    const names = []
    this._players.forEach(p => names.push(p.playerName))
    this._players.forEach(p => {
      p.send(JSON.stringify({action: EVENT_NEW_PLAYER, players: names, room: this._idRoom}))
    })
  }

  _sendAllPlayersValidated() {
    const names = []
    this._players.forEach(p => {
      if (p.hasValidated) {
        names.push(p.playerName)
      }
    })
    this._players.forEach(p => {
      p.send(JSON.stringify({action: EVENT_PLAYER_VALIDATED, players: names}))
    })
  }

  nextShowFinish(socket, request) {
    this._currentNextFinishData++
    if (this._currentNextFinishData === this._players.length) {
      this._startShowNextFinish()
      return
    }
    console.log(`next player ${this._currentFinishPlayerIndex} round ${this._currentNextFinishData}`)

    const master = this._players[this._currentFinishPlayerIndex]
    this._players.forEach(p => {
      if (this._currentNextFinishData === -1) {
        p.send(JSON.stringify({
          action: EVENT_FINISH_NEXT,
          master: master.id,
          playerName: master.playerName,
          data: master.firstDrawing
        }))
      } else {
        const numP = (this._currentNextFinishData + this._playerFinishShowed) % this._players.length
        p.send(JSON.stringify({
          action: EVENT_FINISH_NEXT,
          master: master.id,
          playerName: this._players[numP].playerName,
          data: this._players[numP].datas[this._currentNextFinishData]
        }))
      }
    })
  }

  validDraw(socket, request) {
    if (socket.hasValidated) {
      return
    }
    socket.hasValidated = true

    console.log(request.action, ` ${socket.playerName}`)
    socket.datas.push(request.drawing)
    this._playerFinish++
    if (this._playerFinish === this._players.length) {
      console.log('last player')

      if (this._round + 1 === this._players.length) {
        this._startFinish()
      } else {
        this._startDescription()
      }
    } else {
      this._sendAllPlayersValidated()
    }
  }

  validDescription(socket, request) {
    if (socket.hasValidated) {
      return
    }
    socket.hasValidated = true

    console.log(request.action, ` ${socket.playerName}`)
    socket.datas.push(request.description)

    this._playerFinish++
    if (this._playerFinish === this._players.length) {
      console.log('last player')

      if (this._round + 1 === this._players.length) {
        this._startFinish()
      } else {
        this._startDraw()
      }
    } else {
      this._sendAllPlayersValidated()
    }
  }

  _repCreateRoom(socket) {
    socket.send(JSON.stringify({action: EVENT_RESPONSE_CREATE_ROOM, room: this._idRoom}))
  }

  startGame(socket, request) {
    console.log(`${request.action} ${this._idRoom}`)
    this._ingame = true
    this._shufflePlayers()

    this._players.forEach(p => {
      p.hasValidated = false
      p.firstDrawing = this._randomLabel()
      console.log('player ' + p.playerName + ' draw ' + p.firstDrawing)
      p.send(JSON.stringify({action: EVENT_START_DRAW, description: p.firstDrawing, round: this._round}))
    })
  }

  _randomLabel() {
    const number = Math.floor(Math.random() * this._drawing.length)
    return this._drawing[number]
  }

  players() {
    return this._players
  }

  id() {
    return this._idRoom
  }

  _shufflePlayers() {
    let counter = this._players.length

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      let index = Math.floor(Math.random() * counter)

      // Decrease counter by 1
      counter--

      // And swap the last element with it
      let temp = this._players[counter]
      this._players[counter] = this._players[index]
      this._players[index] = temp
    }
  }

  inGame(){
    return this._ingame
  }
}

module.exports = Game
