let masterName, playerName, tabPlayer, currentFinishId
let cpt = 0, numberPlayer = 0
let canDraw = true
const id = Math.floor(Math.random() * 10e10)

let pathWS
if (window.location.protocol === 'https:') {
  pathWS = `wss://${window.location.hostname}`
} else {
  if (window.location.hostname.startsWith('localhost') ||
    window.location.hostname.startsWith('srv1') ||
    window.location.hostname.startsWith('192.168')) {
    pathWS = `ws://${window.location.hostname}:8002`
  } else {
    window.location.href = window.location.href.replace('http', 'https')
  }
}
console.log(pathWS)

const socket = new WebSocket(pathWS)
socket.onerror = e => {
  console.log('disconnect')
  location.reload()
}
socket.onopen = () => {
  console.log('READY')
  document.getElementById('createRoomBtn').disabled = false
  document.getElementById('joinRoomBtn').disabled = false
}

socket.onmessage = e => {
  const request = JSON.parse(e.data)
  console.log('Request', request.action)
  switch (request.action) {
    case 'allo':
      console.log('ALLO :', request.date)
      break
    case 'rep_create_room':
      repCreateRoom(request)
      break
    case 'validated_player':
      validatedPlayer(request)
      break
    case 'new_player':
      newPlayer(request)
      break
    case 'room_not_found':
      roomNotFound(request)
      break
    case 'start_draw':
      startDraw(request)
      break
    case 'start_description':
      startDescription(request)
      break
    case 'finish_next':
      finishNext(request)
      break
    case 'end_game':
      endGame(request)
      break
    case 'disconnect_player':
      disconnectPlayer(request)
      break
    case 'room_ids':
      roomList(request)
      break
  }
}

init()
document.getElementById('createRoomBtn').addEventListener('click', createRoom)
document.getElementById('joinRoomBtn').addEventListener('click', joinRoom)
document.getElementById('startGameButton').addEventListener('click', startGame)
document.getElementById('buttonDrawZone').addEventListener('click', validationDraw)
document.getElementById('buttonDescription').addEventListener('click', validationFind)
document.getElementById('buttonNext').addEventListener('click', nextStep)

function init() {
  currentFinishId = null
  tabPlayer = []
  cpt = -1
  canDraw = true
  numberPlayer = 0
}

function checkSocket() {
  if (socket.readyState !== 1) {
    alert('Oups, une petite erreur de connexion, rafraichissement')
    location.reload()
    return false
  }
  return true
}

function createRoom() {
  console.log('create room')
  const valid = checkSocket()
  if (valid) {
    setName()
    socket.send(JSON.stringify({action: 'create_room', playerName: playerName, id: id}))
    displayNone('titleScreen')
    displayBlock('startGameButton')
  }
}

function joinRoom() {
  console.log('join room')
  const valid = checkSocket()
  if (valid) {
    setName()
    const idRoom = prompt('Salle ?')
    socket.send(JSON.stringify({action: 'join_room', room: idRoom, playerName: playerName, id: id}))
    displayNone('titleScreen')
  }
}

function setName() {
  const msg = 'Nom ?'
  playerName = prompt(msg)
  while (playerName === null) {
    playerName = prompt(msg)
  }
}

function startGame() {
  console.log('start game')
  socket.send(JSON.stringify({action: 'start_game'}))
}

function validationDraw() {
  console.log('validation draw')
  const validated = confirm('Valider ?')
  if (validated) {
    socket.send(JSON.stringify({action: 'valid_draw', drawing: getImg()}))
    setDisabled('buttonDrawZone', true)
    canDraw = false
  }
}

function validationFind() {
  console.log('validation descr')
  const description = prompt('Que voyez vous ?')
  console.log(description)
  if (description !== null && description.length > 0) {
    socket.send(JSON.stringify({action: 'valid_description', description: description}))
    setDisabled('buttonDescription', true)
  }
}

function nextStep() {
  console.log('next step')
  socket.send(JSON.stringify({action: 'ask_finish_next'}))
}

function repCreateRoom(request) {
  console.log('rep_create_room')
  displayRoomId(request.room)
  numberPlayer = 1
  displayNumberPlayer()
}

function validatedPlayer(request) {
  console.log('validated_player')
  displayNumberValidatedPlayer(request.players.length)
}

function newPlayer(request) {
  console.log('new_player')
  displayRoomId(request.room)
  numberPlayer = request.players.length
  let list = '<ul id="listPersonne">'
  request.players.forEach(p => list += `<li>${p}</li>`)
  list += '</ul>'
  document.getElementById('players').innerHTML = list
  displayBlock('waitingScreen')
  displayNumberPlayer()
}

function displayNumberPlayer() {
  const s = (numberPlayer > 1) ? 's' : ''
  document.getElementById('numberPlayer').innerHTML = `${numberPlayer} joueur${s}`
}

function displayRoomId(id) {
  setText('numberRoom', `Salle ${id}`)
}

function roomNotFound(request) {
  console.log('room_not_found')
  alert('Cette salle n\'existe pas !')
  displayBlock('titleScreen')
}

function startDraw(request) {
  console.log('start_draw')
  canDraw = true
  displayNumberValidatedPlayer(0)
  displayNumberRound(request.round)
  displayNone('footerGGJ')
  displayNone('waitingScreen')
  displayNone('imageContainer')
  displayBlock('drawZone')
  setDisabled('buttonDrawZone', false)
  startDrawing()
  setText('titleDrawZone', request.description)
}

function startDescription(request) {
  console.log('start_description')
  displayNumberValidatedPlayer(0)
  displayNumberRound(request.round)
  displayNone('drawZone')
  displayBlock('imageContainer')
  setDisabled('buttonDescription', false)
  setAttribute('imageToFind', 'src', request.drawing)
}

function finishNext(request) {
  console.log('finish_next')
  displayNone('drawZone')
  displayNone('imageContainer')
  displayBlock('imageContainerEnd')
  displayBlock('endPlayerName')

  if (currentFinishId !== request.master) {
    cpt = -1
    currentFinishId = request.master
  }

  if (id !== request.master) {
    displayNone('containerButtonNext')
  } else {
    displayBlock('containerButtonNext')
  }

  if (cpt === -1) {
    displayNone('imageDraw')
    displayBlock('labelImage')
    setText('endPlayerName', `${request.playerName} devait dessiner`)
    setText('labelImage', request.data)
  } else {
    if (cpt % 2 === 0) {
      displayNone('labelImage')
      displayBlock('imageDraw')
      setText('endPlayerName', `${request.playerName} a dessiné`)
      setAttribute('imageDraw', 'src', request.data)
    } else {
      displayNone('imageDraw')
      displayBlock('labelImage')
      setText('endPlayerName', `${request.playerName} pense que c'est`)
      setText('labelImage', request.data)
    }
  }

  cpt++
}

function endGame(request) {
  console.log('end_game')
  displayBlock('titleScreen')
  displayBlock('footerGGJ')
  displayNone('waitingScreen')
  displayNone('drawZone')
  displayNone('imageContainer')
  displayNone('imageContainerEnd')
  init()
}

function disconnectPlayer(request) {
  alert(`Le joueur ${request.playerName} a été déconnecté`)
  endGame()
}

function displayBlock(id) {
  document.getElementById(id).style.display = 'block'
}

function displayNone(id) {
  document.getElementById(id).style.display = 'none'
}

function setText(id, text) {
  document.getElementById(id).innerText = text
}

function setAttribute(id, attribute, val) {
  document.getElementById(id).setAttribute(attribute, val)
}

function setDisabled(id, val) {
  document.getElementById(id).disabled = val
}

function displayNumberValidatedPlayer(number) {
  const val = `<h4 class="numberPlayerValidated">Joueurs pret : ${number}/${numberPlayer} </h4>`
  document.getElementById('numberPlayerDescZone').innerHTML = val
  document.getElementById('numberPlayerDrawZone').innerHTML = val
}

function displayNumberRound(round) {
  const val = `<h4 class="roundIndicator">Tours : ${round + 1}/${numberPlayer}</h4>`
  document.getElementById('roundIndicatorDrawZone').innerHTML = val
  document.getElementById('roundIndicatorDescZone').innerHTML = val
}

function roomList(request) {
  console.log('room list', request.rooms)
  // Todo y a un truc a faire ici de pas mal
  if (request.rooms.length > 0) {
    let list = '<ul id="listRoom">'
    request.rooms.forEach(p => list += `<li>${p}</li>`)
    list += '</ul>'
    document.getElementById('roomList').innerHTML = list
  }
}
