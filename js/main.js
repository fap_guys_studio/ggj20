function startDrawing() {
  const canvas = document.getElementById("canvas");
  const context = canvas.getContext("2d");
  canvas.width = canvas.getBoundingClientRect().width
  canvas.height = canvas.getBoundingClientRect().height
  context.strokeStyle = "black";
  context.lineWidth = 1.5;
  context.lineCap = "round";
  let disableSave = true;
  let pixels = [];
  let cpixels = [];
  let xyLast = {};
  let xyAddLast = {};
  let calculate = false;
  {   //functions
    function remove_event_listeners() {
      canvas.removeEventListener('mousemove', on_mousemove, false);
      canvas.removeEventListener('mouseup', on_mouseup, false);
      canvas.removeEventListener('touchmove', on_mousemove, false);
      canvas.removeEventListener('touchend', on_mouseup, false);

      document.body.removeEventListener('mouseup', on_mouseup, false);
      document.body.removeEventListener('touchend', on_mouseup, false);
    }

    function get_coords(e) {
      let x, y;

      if (e.changedTouches && e.changedTouches[0]) {
        let offsety = canvas.offsetTop || 0;
        let offsetx = canvas.offsetLeft || 0;

        x = e.changedTouches[0].pageX - offsetx;
        y = e.changedTouches[0].pageY - offsety;
      } else if (e.layerX || 0 == e.layerX) {
        x = e.layerX;
        y = e.layerY;
      } else if (e.offsetX || 0 == e.offsetX) {
        x = e.offsetX;
        y = e.offsetY;
      }

      return {
        x : x, y : y
      };
    };

    function on_mousedown(e) {
      e.preventDefault();
      e.stopPropagation();

      canvas.addEventListener('mouseup', on_mouseup, false);
      canvas.addEventListener('mousemove', on_mousemove, false);
      canvas.addEventListener('touchend', on_mouseup, false);
      canvas.addEventListener('touchmove', on_mousemove, false);
      document.body.addEventListener('mouseup', on_mouseup, false);
      document.body.addEventListener('touchend', on_mouseup, false);

      if (canDraw) {
        empty = false;
        let xy = get_coords(e);
        context.beginPath();
        pixels.push('moveStart');
        context.moveTo(xy.x, xy.y);
        pixels.push(xy.x, xy.y);
        xyLast = xy;
      }
    };

    function on_mousemove(e, finish) {
      e.preventDefault();
      e.stopPropagation();
      if (canDraw) {
        let xy = get_coords(e);
        let xyAdd = {
          x : (xyLast.x + xy.x) / 2,
          y : (xyLast.y + xy.y) / 2
        };

        if (calculate) {
          let xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
          let yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
          pixels.push(xLast, yLast);
        } else {
          calculate = true;
        }

        context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
        pixels.push(xyAdd.x, xyAdd.y);
        context.stroke();
        context.beginPath();
        context.moveTo(xyAdd.x, xyAdd.y);
        xyAddLast = xyAdd;
        xyLast = xy;
      }
    };

    function on_mouseup(e) {
      remove_event_listeners();
      disableSave = false;
      context.stroke();
      pixels.push('e');
      calculate = false;
    };
  }
  canvas.addEventListener('touchstart', on_mousedown, false);
  canvas.addEventListener('mousedown', on_mousedown, false);
}

function getImg() {
  const canvas = document.getElementById("canvas");// save canvas image as data url (png format by default)
  const dataURL = canvas.toDataURL("image/png");
  return dataURL
}

function clearImg() {
  const canvas = document.getElementById("canvas");
  const context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height);
}
