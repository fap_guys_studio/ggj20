const fs = require('fs')
const Game = require('./server/Game')
const express = require('express')
const app = express()
const WebSocket = require('ws')
const server = require('http').Server(app)
const DEBUG = '*'

const MAX_ROOM = 10

app.use('/css', express.static(__dirname + '/css'))
app.use('/js', express.static(__dirname + '/js'))
app.use('/assets', express.static(__dirname + '/assets'))

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html')
})

const wss = new WebSocket.Server({server: server})

const drawing = JSON.parse(fs.readFileSync('./server/drawing.json')).drawing

server.listen(process.env.PORT || 8002, function () {
  console.info('Listening on ' + server.address().port)
})

const games = new Map()
const sockets = []
const socketsInGame = []
const socketsWaiting = []

const EVENT_CREATE_ROOM = 'create_room'
const EVENT_JOIN_ROOM = 'join_room'
const EVENT_START_GAME = 'start_game'
const EVENT_VALID_DRAW = 'valid_draw'
const EVENT_VALID_DESCRIPTION = 'valid_description'
const EVENT_ASK_FINISH_NEXT = 'ask_finish_next'
const EVENT_ROOM_IDS = 'room_ids'

setInterval(() => {
  games.forEach(g => {
    if (g.players().length === 0) {
      console.log('remove room ' + g.id())
      games.delete(g.id())
    }
  })
}, 2000)

wss.on('connection', (socket) => {
  console.log('connection')
  sockets.push(socket)
  socketsWaiting.push(socket)

  socket.intervalId = setInterval(() => {
    socket.send(JSON.stringify({action: 'allo', date: Date.now().toString()}))
  }, 20000)

  sendRoomList()

  socket.on('message', e => {
    const request = JSON.parse(e)
    console.log(request.action)

    switch (request.action) {
      case EVENT_CREATE_ROOM:
        createRoom(socket, request)
        break
      case EVENT_JOIN_ROOM:
        joinRoom(socket, request)
        break
      case EVENT_VALID_DRAW:
        validDraw(socket, request)
        break
      case EVENT_VALID_DESCRIPTION:
        validDescription(socket, request)
        break
      case EVENT_ASK_FINISH_NEXT:
        askFinishNext(socket, request)
        break
      case EVENT_START_GAME:
        startGame(socket, request)
        break
    }
  })

  socket.on('close', (e) => {
    deleteSocket(socket)

    clearInterval(socket.intervalId)
    console.log(`disconnect player : ${socket.playerName} room : ${socket.room}`)
    if (socket.room && games.get(socket.room)) {
      games.get(socket.room).removePlayer(socket)
    }
  })

  socket.on('error', (error) => {
    deleteSocket(socket)
    console.error('error', error)
    console.log('error', error)
  })
})

function createRoom(socket, request) {
  socketGoGame(socket)

  let idRoom = '' + Math.floor(Math.random() * MAX_ROOM)
  while (games.has(idRoom)) {
    idRoom = '' + Math.floor(Math.random() * MAX_ROOM)
  }

  socket.playerName = request.playerName
  socket.id = request.id
  const game = new Game(drawing, socket, idRoom)
  games.set(idRoom, game)

  sendRoomList()
}

function joinRoom(socket, request) {
  socketGoGame(socket)
  socket.playerName = request.playerName

  if (games.has(request.room)) {
    const game = games.get(request.room)
    socket.idRoom = request.room
    socket.id = request.id

    game.addPlayer(socket)
  } else {
    socket.send(JSON.stringify({action: 'room_not_found'}))
  }
}

function validDraw(socket, request) {
  games.get(socket.room).validDraw(socket, request)
}

function validDescription(socket, request) {
  games.get(socket.room).validDescription(socket, request)
}

function askFinishNext(socket, request) {
  games.get(socket.room).nextShowFinish(socket, request)
}

function startGame(socket, request) {
  games.get(socket.room).startGame(socket, request)
}


function removeSocket(list, socket) {
  const number = list.indexOf(socket)
  list.splice(number, 1)
}

function deleteSocket(socket) {
  removeSocket(sockets, socket)
  removeSocket(socketsInGame, socket)
  removeSocket(socketsWaiting, socket)
}

function socketGoGame(socket) {
  removeSocket(sockets, socket)
  removeSocket(socketsWaiting, socket)
  socketsInGame.push(socket)
}

function sendRoomList() {
  const rooms = []
  games.forEach(game => {
    if (!game.inGame()) {
      rooms.push(game.id())
    }
  })
  socketsWaiting.forEach(socket => {
    socket.send(JSON.stringify({action: EVENT_ROOM_IDS, rooms: rooms, roomTotal: games.size}))
  })
}
